/*
 * Copyright(c) 2007 - 2019 Realtek Corporation. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include "spi_api.h"
#include "spi_ex_api.h"
#include "wait_api.h"
#include "sys_api.h"

#define SPI_IS_AS_MASTER    0 /*1*/
#define TEST_BUF_SIZE       16 /*2048*/
#define SCLK_FREQ           1000000

#define SPI0_MOSI           PA_19
#define SPI0_MISO           PA_20
#define SPI0_SCLK           PA_3
#define SPI0_CS             PA_2

extern void hal_ssi_toggle_between_frame(phal_ssi_adaptor_t phal_ssi_adaptor, u8 ctl);

void dump_data (const u8 *start, u32 size, char * strHeader)
{
    int row, column, index, index2, max;
    u8 *buf, *line;

    if (!start || (size == 0)) {
        return;
    }

    line = (u8*)start;

    //16 bytes per line
    if (strHeader) {
        dbg_printf("%s", strHeader);
    }

    column = size % 16;
    row = (size / 16) + 1;
    for (index = 0; index < row; index++, line += 16) {
        buf = (u8*)line;

        max = (index == row - 1) ? column : 16;
        if (max == 0) {
            break; /* If we need not dump this line, break it. */
        }

        dbg_printf("\r\n[%08x] ", line);

        //Hex
        for (index2 = 0; index2 < max; index2++) {
            if (index2 == 8) {
                dbg_printf("  ");
            }
            dbg_printf("%02x ", (u8)buf[index2]);
        }

        if (max != 16) {
            if (max < 8) {
                dbg_printf("  ");
            }
            for ((index2 = 16 - max); index2 > 0; index2--) {
                dbg_printf("   ");
            }
        }
    }

    dbg_printf("\r\n");
    return;
}

char TestBuf[TEST_BUF_SIZE + 1024]; /* +1024 to allow overflow a few bytes */
volatile int TrDone;

void master_tr_done_callback (void *pdata, SpiIrq event)
{
    TrDone = 1;
}

void slave_tr_done_callback (void *pdata, SpiIrq event)
{
    TrDone = 1;
}

#if SPI_IS_AS_MASTER
spi_t spi_master;
#else
spi_t spi_slave;
#endif

int spi_test_stream_main_init (void)
{
    dbg_printf("\r\n   SPI Stream DMA Twoboard DEMO init \r\n");

    if ((SPI0_SCLK == PA_3) || (SPI0_CS == PA_2)) {
        sys_jtag_off();
    }

#if SPI_IS_AS_MASTER
  #error master code removed
#else
    spi_init(&spi_slave, SPI0_MOSI, SPI0_MISO, SPI0_SCLK, SPI0_CS);
    spi_format(&spi_slave, DfsSixteenBits + 1, ((int)SPI_SCLK_IDLE_LOW | (int)SPI_SCLK_TOGGLE_MIDDLE), 1);
    hal_ssi_toggle_between_frame(&spi_slave.hal_ssi_adaptor, ENABLE);
    spi_irq_hook(&spi_slave, (spi_irq_handler)slave_tr_done_callback, (uint32_t)&spi_slave);
    spi_disable(&spi_slave);

    dbg_printf("\r\n   SPI Stream DMA Twoboard DEMO  inited \r\n");
    return 0;
}

int spi_test_stream_main_recv (int refn)
{
    int i, stopped;

    memset(TestBuf, 0, TEST_BUF_SIZE);
    dbg_printf("SPI Slave Read Test ==> %d \r\n", refn);
    TrDone = 0;
    spi_enable(&spi_slave);
    spi_flush_rx_fifo(&spi_slave);
    spi_slave_read_stream_dma(&spi_slave, TestBuf, TEST_BUF_SIZE);
    i = 0;
    dbg_printf("SPI Slave Wait Read Done... \r\n");
    dbg_printf("    spi adaptor rx_length %u\r\n", spi_slave.hal_ssi_adaptor.rx_length);
    stopped = 0;
    while (TrDone == 0) {
        wait_ms(100);
        i++;
        if (i == 150) {
            hal_ssi_stop_recv(&spi_slave.hal_ssi_adaptor); /* stop done callback */
            stopped = 1; /* early abort due to timeout */
            u32 busy = (spi_slave.state & SPI_STATE_RX_BUSY);
            if ( busy )
                spi_slave.state &= ~SPI_STATE_RX_BUSY;
            spi_disable(&spi_slave); /* or else the rx_length will overflow in non-dma recv */
            //dbg_printf("    busy %u\r\n", busy);
            dbg_printf("SPI Slave Wait Timeout \r\n");
        }
        if (i > 300) {
            dbg_printf("SPI Slave Wait Timeout 2\r\n");
            break;
        }
    }
    dbg_printf("    spi adaptor rx_length %u\r\n", spi_slave.hal_ssi_adaptor.rx_length);
    dump_data((const u8 *)TestBuf, TEST_BUF_SIZE, "SPI Slave Read Data:");

    if ( stopped ) {
        u32 rlen = spi_slave.hal_ssi_adaptor.rx_length;
        if ( TrDone == 0 ) {
            if (rlen <= TEST_BUF_SIZE) {
                dbg_printf("    spi partial rx %u\r\n", TEST_BUF_SIZE-rlen);
            } else {
                dbg_printf("    spi partial rx error rlen %u\r\n", rlen);
            }
        } else {
            dbg_printf("    spi partial rx error rlen %u\r\n", rlen);
        }
    } else {
        u32 rlen = spi_slave.hal_ssi_adaptor.rx_length;
        dbg_printf("    spi full rx rlen %u\r\n", rlen);
    }

    //spi_free(&spi_slave);
    dbg_printf("SPI Demo finished. %d \r\n", refn);
    //for(;;);
    return 0;
}

