#include "FreeRTOS.h"
#include "task.h"
#include "diag.h"
#include "main.h"
#include <example_entry.h>

extern void console_init(void);
extern int spi_test_stream_main_init(void);
extern int spi_test_stream_main_recv(int);

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
	spi_test_stream_main_init();
	/*spi_test_stream_main_recv(1);*//*ok*/
	/*spi_test_stream_main_recv(2);*//*ok*/
	/* Initialize log uart and at command service */
	console_init();

	/* pre-processor of application example */
	pre_example_entry();

	/*spi_test_stream_main_recv(3);*//*no rx done callback. it comes late.*/

	/* wlan intialization */
	wlan_network();

	/* Execute application example */
	example_entry();

	/*spi_test_stream_main_recv(4);*//*ok*/

	/* Enable Schedule, Start Kernel */
	vTaskStartScheduler();

	/* Should NEVER reach here */
	return 0;
}
